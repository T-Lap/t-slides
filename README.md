# T-Slides

Bash script to create a Slide-show in your Terminal.

---

### Dependencies:

- bash
- tput

---

## What's about T-Slides

T-Slides is a simple bash script which take a simplified markdown file and create a alone standing bash script witch run your slide presentation.

## How to use

### Markdown

You can use the following markdown commands:

```
---  You need to start a new page/slide with three dashes.
--  Tow dashes will give you a centered horizontal line.
#  ##  ### for headers starting at 1/4 of the screen width.
!#  !##  !### for a centered header.
!  an exclamation mark followed by space give you a centered text block.
- one dash draw a bullet point at the start of the line.
> a greater then sign and space create a block quote.
&  set a brack point and wait for any input to draw the next element.
```

### Creating and use of the Presentation

You just run the t-slides script with the markdown file as argument.

```./t-slides Title.md```

The script ask you for a accent color. At a 256 color terminal you can chose any of this by just enter the number.

> TIP: use `for i in {1..256};do tput setab $i ;echo -n " $i ";done` to see your colors.

The presentation will start automatically and you can use the arrow keys or h j k l to change the slides. Whith q you exit the slide show.
The created an other bash script at you `pwd` with the same name as your markdown file. This you can run with `bash your-slide` or make it executable and share. It will run without the main script.
When you do corrections on your makdown file and run `t-slides`, this will over write the old output file.

> You can also change your slides by editing the output file. For example the accent-color.

---
Have fun

